"use strict";
const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { VueLoaderPlugin } = require("vue-loader");


let pathsToClean = [
  'build'
]

// the clean options to use
let cleanOptions = {
  root: __dirname,
  verbose: false, // Write logs to console.
  dry: false
}

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve( __dirname, '../../assets' ),
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    publicPath: '/',
},

  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {   
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: "vue-loader"
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },

  plugins: [
 
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    /*new HtmlWebPackPlugin({
      template: "./index.php",
      filename: "./index.php",
      favicon: "./favicon.ico"
    }),*/
    new VueLoaderPlugin(),
   /* new CopyWebpackPlugin([
      { from: "src/assets/img", to: "assets/img" },
      { from: "src/assets/fonts", to: "assets/fonts" }
    ])
   */
  
  ]
};
