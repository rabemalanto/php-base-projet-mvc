'use strict'

const merge = require('webpack-merge')
const baseConfig = require('./base')


module.exports = merge(baseConfig, {
  mode: 'production',
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "all",
        },
      },
    },
  },
  optimization: {
    minimizer: [
    ]
  },

  module: {
    rules: [
      
    ]
  },
  plugins: [
  ]
})
