'use strict'
const { VueLoaderPlugin } = require('vue-loader')
module.exports = {
  mode: 'development',
  entry: [
    './src/main.js'
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      }
    ]
  },
  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {   
    }
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}