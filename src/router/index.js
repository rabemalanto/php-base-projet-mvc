import Vue from 'vue'
import Router from 'vue-router'

import AuthRequired from './authRequired'
Vue.use(Router);
import App from '../component/App.vue'
import Login from '../component/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      beforeEnter:AuthRequired,
      name: 'home',
      component: App
    },
    {
      name: 'login',
      path: '/login',
      component: Login
    }
  ]
})
